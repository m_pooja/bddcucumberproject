Feature: Trigger Post API

Scenario Outline: Trigger the Post API request with valid request parameters

       Given Enrter "<Name>" and "<Job>" in post request body
       When Send the post request with data
        Then Validate data_driven_post status code 
        And Validate data_driven_post response body parameters
Examples:
        |Name |Job |
        |Tamohara|QA|
        |Tungesha|SrQA|
        |Pooja|Dev|        