Feature: Trigger put API

Scenario Outline: Trigger the Put API request with valid request parameters

       Given Enrter "<Name>" and "<Job>" in put API request body
       When Send the put request with API data driven
        Then Validate data_driven_put status code 
        And Validate API data_driven_put response body parameters
Examples:
        |Name |Job |
        |Mahesh|CO|
        |Isha|SrQA|
        |Guru|QA|  