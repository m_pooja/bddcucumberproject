Feature: Trigger patch API

Scenario Outline: Trigger the Patch API request with valid request parameters

       Given Enrter "<Name>" and "<Job>" in patch API request body
       When Send the patch request with API data driven
        Then Validate data_driven patch API status code 
        And Validate API data driven patch response body parameters
Examples:
        |Name |Job |
        |Nidhi|Lead|
        |Mani|Manager|
        |Lingu|QA|  