package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.junit.Assert;

import API_common_methods.common_method_handle_API;
import Endpoint.put_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.put_request_repository;
import utility_common_method.handle_api_logs;
import utility_common_method.handle_directory;

public class PutAPI_DataDriven_StepDefinition {
	String requestBody;
	int statusCode;
	String responseBody;
	String endpoint;
	File log_dir;

	@Given("Enrter {string} and {string} in put API request body")
	public void enrter_and_in_put_api_request_body(String req_name, String req_job) {
		log_dir = handle_directory.creater_log_directory("put_tc1_logs");
		requestBody = requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job
				+ "\"\r\n" + "}";
		endpoint = put_endpoint.put_endpoint_tc1();
		
	}

	@When("Send the put request with API data driven")
	public void send_the_request_with_valid_put_payload() throws IOException {
		statusCode = common_method_handle_API.Put_statuscode(requestBody, endpoint);
		responseBody = common_method_handle_API.Put_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		handle_api_logs.evidence_creator(log_dir, "put_tc1", endpoint, requestBody, responseBody);
	}

	@Then("Validate data_driven_put status code")
	public void validate_put_status_code() {
		Assert.assertEquals(statusCode, 200);
	}

	@Then("Validate API data_driven_put response body parameters")
	public void validate_put_response_body_parameter() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdat = jsp_res.getString("updatedAt");
		res_createdat = res_createdat.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdat, expecteddate);
		System.out.println("Put Response Body Validation Successfull");
	}

}
