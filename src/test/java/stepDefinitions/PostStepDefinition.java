package stepDefinitions;

import static io.restassured.RestAssured.given;

import java.io.IOException;

import org.junit.Assert;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import request_repository.post_request_repository;

public class PostStepDefinition {
	String requestBody;
	int statuscode;
	String responsebody;

	@Given("Enrter NAME and JOB in request body")
	public void enrter_name_and_job_in_request_body() throws IOException {
		String BaseURI = "https://reqres.in/";
		RestAssured.baseURI = BaseURI;
		requestBody = post_request_repository.post_request_tc1();
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@When("Send the request with payload")
	public void send_the_request_with_payload() {
		statuscode = given().header("Content-Type", "application/json").body(requestBody).when().post("/api/users")
				.then().extract().statusCode();
		responsebody = given().header("Content-Type", "application/json").body(requestBody).when().post("/api/users")
				.then().extract().response().asString();
		System.out.println(responsebody);
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate status code")
	public void validate_status_code() {
		Assert.assertEquals(statuscode, 201);
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate response body parameter")
	public void validate_response_body_parameter() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		JsonPath jspresponse = new JsonPath(responsebody);
		String res_name = jspresponse.getString("name");
		String res_job = jspresponse.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Response Body Validation Successfull");
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

}
