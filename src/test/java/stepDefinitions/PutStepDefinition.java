package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.junit.Assert;

import API_common_methods.common_method_handle_API;
import Endpoint.put_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.put_request_repository;
import utility_common_method.handle_api_logs;
import utility_common_method.handle_directory;

public class PutStepDefinition {
	String requestBody;
	int statusCode;
	String responseBody;
	String endpoint;
	File log_dir;

	@Given("Enrter NAME and JOB in put request body")
	public void enrter_name_and_job_in_put_request_body() throws IOException {
		log_dir = handle_directory.creater_log_directory("put_tc1_logs");
		requestBody = put_request_repository.put_request_tc1();
		endpoint = put_endpoint.put_endpoint_tc1();
	}

	@When("Send the request with valid put payload")
	public void send_the_request_with_valid_put_payload() throws IOException {
		statusCode = common_method_handle_API.Put_statuscode(requestBody, endpoint);
		responseBody = common_method_handle_API.Put_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		handle_api_logs.evidence_creator(log_dir, "put_tc1", endpoint, requestBody, responseBody);
	}

	@Then("Validate put status code")
	public void validate_put_status_code() {
		Assert.assertEquals(statusCode, 200);
	}

	@Then("Validate put response body parameter")
	public void validate_put_response_body_parameter() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdat = jsp_res.getString("updatedAt");
		res_createdat = res_createdat.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdat, expecteddate);
		System.out.println("Put Response Body Validation Successfull");
	}

}
