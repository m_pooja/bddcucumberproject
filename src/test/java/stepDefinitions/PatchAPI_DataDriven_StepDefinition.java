package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.junit.Assert;

import API_common_methods.common_method_handle_API;
import Endpoint.patch_endpoint;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.patch_request_repository;
import utility_common_method.handle_api_logs;
import utility_common_method.handle_directory;

public class PatchAPI_DataDriven_StepDefinition {
	String requestBody;
	int statusCode;
	String responseBody;
	String endpoint;
	File log_dir;
	
	@Before
	public void setup() {
		System.out.println("\n-------APi Triggered-------");
	}
	@After
	public void teardown() {
		System.out.println("-------Execution Successfull-------");
	}

	@Given("Enrter {string} and {string} in patch API request body")
	public void enrter_and_in_patch_api_request_body(String req_name, String req_job) {
		log_dir = handle_directory.creater_log_directory("patch_tc1_logs");
		requestBody = requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job
				+ "\"\r\n" + "}";
		endpoint = patch_endpoint.patch_endpoint_tc1();

	}

	@When("Send the patch request with API data driven")
	public void send_the_patch_request_with_payload() throws IOException {
		statusCode = common_method_handle_API.Patch_statuscode(requestBody, endpoint);
		responseBody = common_method_handle_API.Patch_responseBody(requestBody, endpoint);
		System.out.println(responseBody);

	}

	@Then("Validate data_driven patch API status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(statusCode, 200);
	}

	@Then("Validate API data driven patch response body parameters")
	public void validate_patch_response_body_parameter() throws IOException {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("updatedAt");
		res_createdate = res_createdate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdate, expecteddate);
		handle_api_logs.evidence_creator(log_dir, "patch_tc1", endpoint, requestBody, responseBody);
	}

}
