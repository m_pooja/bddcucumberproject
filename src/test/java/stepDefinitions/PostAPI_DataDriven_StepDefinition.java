package stepDefinitions;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;

import Endpoint.post_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import request_repository.post_request_repository;
import utility_common_method.handle_directory;

public class PostAPI_DataDriven_StepDefinition {
	String requestBody;
	int statuscode;
	String responsebody;
	//String endpoint;
	//File log_dir;

	@Given("Enrter {string} and {string} in post request body")
	public void enrter_and_in_post_request_body(String req_name, String req_job) throws IOException {
		String BaseURI = "https://reqres.in/";
		RestAssured.baseURI = BaseURI;
		requestBody = requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job
				+ "\"\r\n" + "}";

	}

	@When("Send the post request with data")
	public void send_the_request_with_payload() {
		statuscode = given().header("Content-Type", "application/json").body(requestBody).when().post("/api/users")
				.then().extract().statusCode();
		responsebody = given().header("Content-Type", "application/json").body(requestBody).when().post("/api/users")
				.then().extract().response().asString();
		System.out.println(responsebody);
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate data_driven_post status code")
	public void validate_status_code() {
		Assert.assertEquals(statuscode, 201);
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

	@Then("Validate data_driven_post response body parameters")
	public void validate_response_body_parameter() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		JsonPath jspresponse = new JsonPath(responsebody);
		String res_name = jspresponse.getString("name");
		String res_job = jspresponse.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Response Body Validation Successfull");
		// Write code here that turns the phrase above into concrete actions
		// throw new io.cucumber.java.PendingException();
	}

}
